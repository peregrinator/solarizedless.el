# solarized-light with `syntax off` 

A nearly colourless theme based on Ethan Schoonover's
[solarized](https://github.com/solarized). Heavily inspired by
[u/n4rw00's](htpps://reddit.com/user/n4rwoo) [Reddit
post](https://www.reddit.com/r/unixporn/comments/fzrglg/dwm_fell_off_the_tmux_bandwagon/)
which features a similar theme for `vis-editor` (reminder to self: TRY IT
TODAY). This uses my fork of the
[`colourless-themes`](https://github.com/peregrinat0r/colourless-themes.el)
macro.

## Screenshot(s)

![scrot running doom-emacs in urxvt](screenshots/scrot.png)

## Installation and usage
*work in progress*

## Terminal colours (WIP)

Also included is a terminal colour theme with nearly identical colours 
